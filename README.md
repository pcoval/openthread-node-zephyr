# Gateway Blueprint OpenThread Node Zephyr Application

[[_TOC_]]

## Overview

A Zephyr application demonstrating the basic configuration to join and
participate in an OpenThread mesh network created by the Gateway blueprint of
the Oniro Project.

### Requirements
- Supported boards:
  - Arduino Nano 33 BLE

## Building and flashing
### Building and flashing with Zephyr SDK
With the Zephyr SDK installed on your build machine using west for building and
flashing is the easiest option:

```
$ west build -p auto -b arduino_nano_33_ble . && west flash
```
